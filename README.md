This code follows https://arxiv.org/abs/1408.3534.pdf in the calculation of the gravitational wave signal of an Intermediate Mass Ratio Insprial(IMRI) inside a dark matter density spike

    halo.py 
This file gives basic classes for dark matter halos, their density and mass distribution

    merger_system.py
This file defines a class that encapsulates the properties of the merging system such as the respective masses of the objects, 
 the luminosity distance to us and the halo encasing the primary mass m1. 
It also defines some constants that are useful for the conversions, as the code is written in natural units G=c=1

    inspiral.py
This file contains the solver for the differential equation that models the inspiral

    waveform.py 
This file contains the method to calculate the gravitational wave signal in its most simple form

    detector.py 
This file contains classes for detectors and their sensitivity. Also allows to calculate the signal to noise ratio.

    cosmo.py 
This file contains a few methods to calculate cosmological quantities, such as redshift, critical density and Omega_m.

    crosschecks9402014.py
This file models the inspiral without any dark matter halo and compares to the analytic results given in https://arxiv.org/pdf/gr-qc/9402014.pdf

    crosschecks1408.3534.py
This file models the inspiral with a dark matter halo and compares to the semianalytic equations given in https://arxiv.org/abs/1408.3534.pdf

    reproduction1408.3534.py
This file reproduces some of the results of https://arxiv.org/abs/1408.3534.pdf
