import numpy as np
import cosmo
import halo
#from scipy.constants import G, c


hz_to_invpc = 1.029e8
s_to_pc = 9.716e-9
m_to_pc = 3.241e-17
solar_mass_to_pc = 4.8e-14
g_cm3_to_invpc2 = 7.072e8
year_to_pc = 0.3064



class SystemProp:

    def __init__(self, m1, m2, D, halo):
        self.m1 = m1
        self.m2 = m2
        self.m_red = m1*m2/(m1+m2)
        self.m_tot = m1+m2
        self.m_chirp = self.m_red**(3./5.) * self.m_tot**(2./5.)

        self.D = D
        self.z = cosmo.HubbleLaw(D)

        self.m_red_redshifted = (1.+self.z) * self.m_red
        self.m_chirp_redshifted = (1.+self.z) * self.m_chirp

        self.r_isco = 6.*self.m1

        self.halo = halo
        self.halo.r_min = self.r_isco

    def mass(self, r):
        return np.ones(np.shape(r))*self.m1 + self.halo.mass(r)

    def omega_s(self, r):
        return np.sqrt((self.mass(r) + self.m2)/r**3)

    def omega_s_approx(self, r):
        return np.sqrt((self.m1)/r**3)
