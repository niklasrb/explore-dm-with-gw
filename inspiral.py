import numpy as np
from scipy.integrate import solve_ivp, quad, trapz, simps
from scipy.special import ellipeinc, ellipe, ellipkinc
import merger_system as ms
import collections
#import sys
import time



def dE_orbit_dR(R, sp):
    return sp.m_red/2.*((sp.mass(R) + sp.m2)/R**2 - 4.*np.pi*sp.halo.density(R)*R)

def dE_gw_dt(R, sp):
    omega_s = sp.omega_s(R)
    return 32./5. * sp.m_red**2 * R**4 * omega_s**6

def dE_df_dt(R, sp):
    v_s = sp.omega_s(R)*R
    ln_Lambda = 3.
    return 4.*np.pi *sp.m2**2 * sp.halo.density(R) * ln_Lambda / v_s

def dR_dt(R, sp):
        return -(dE_gw_dt(R, sp) + dE_df_dt(R, sp)) / dE_orbit_dR(R, sp)


def evolve_circular_binary(sp, R_0, R_fin=0., t_0=0., acc=1e-8):
    # If a list of R_fin is passed, then the integration is performed multiple times
    # This allows to cover multiple orders of magnitude without sacrificing performance/accuracy
    if isinstance(R_fin, collections.Sequence):
        t= [0.]; R = [R_0]; omega_s = [sp.omega_s(R_0)];                                        # set starting point
        for i in range(len(R_fin)):
            ti, Ri, omega_si = evolve_circular_binary(sp, R[-1], R_fin[i], t[-1])
            ti = np.delete(ti, 0); Ri = np.delete(Ri, 0); omega_si = np.delete(omega_si, 0)     # remove double entries
            t = np.append(t, ti); R = np.append(R, Ri); omega_s = np.append(omega_s, omega_si)
        return t, R, omega_s

    #R_fin = np.max(sp.r_isco, R_fin)
    t_coal =  5./256. * R_0**4/sp.m_tot**2 /sp.m_red
    t_fin = 3./2.*t_coal *( 1. - R_fin**4 / R_0**4)
    t_step_max = t_fin/1e4
    print("Evolving from ", R_0/sp.r_isco, " to ", R_fin/sp.r_isco,"r_isco with maximum step size ", t_step_max)

    dR_dtunitless = lambda t, y, *args: t_coal/R_fin *dR_dt(y[0]*R_fin, args[0])

    fin_reached = lambda t,y, *args: y[0] - 1.
    fin_reached.terminal = True

    Int = solve_ivp(dR_dtunitless, [t_0/t_coal, (t_0+t_fin)/t_coal], [R_0/R_fin], dense_output=True, args=([sp]), events=fin_reached, max_step=t_step_max/t_coal, \
                                                                                    method = 'LSODA', atol=acc, rtol=acc)

    R = Int.y[0]*R_fin
    t = Int.t*t_coal

    print(" -> Evolution took ", "{0:.4e}".format((t[-1] - t[0])/ms.year_to_pc), " yrs")
    return t, R, sp.omega_s(R)


