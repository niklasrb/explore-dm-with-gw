import numpy as np
from scipy.interpolate import UnivariateSpline, interp1d
from scipy.integrate import quad
import matplotlib.pyplot as plt
import merger_system as ms
import halo
import inspiral
import waveform
import detector


sp = ms.SystemProp(1e3 *ms.solar_mass_to_pc, 1. * ms.solar_mass_to_pc, 1., halo.DMHalo(0.))

R0= 100.*sp.r_isco
R_fin = sp.r_isco
#R_fin =  [15.*sp.r_isco, sp.r_isco]

t, R, omega_s = inspiral.evolve_circular_binary(sp, R0, R_fin, acc=1e-8)

f_gw = omega_s/np.pi
f_isco = f_gw[-1]
t_of_f = interp1d(omega_s/np.pi, t, kind='cubic', bounds_error=False, fill_value='extrapolate')
omega_gw = interp1d(t, 2*omega_s, kind='cubic', bounds_error=False, fill_value='extrapolate')

t_c = t[-1] - t[0] + 5./256. * R[-1]**4/sp.m_tot**2 / sp.m_red
t_c0 = 5./256. *R0**4 / sp.m_tot**2 / sp.m_red
omega_isco0 = np.sqrt((sp.m1+sp.m2)/sp.r_isco**3)
print(t_c0, t_c, t_c/t_c0 - 1.)
print(omega_isco0, omega_s[-1], omega_s[-1]/omega_isco0 - 1.)
print(sp.r_isco, R[-1], R[-1]/sp.r_isco - 1.)

plt.axvline(omega_isco0/np.pi/ms.hz_to_invpc, linestyle='--', color='red', label='$f_{isco}^{analytic}$')    # 
plt.axvline(f_isco/ms.hz_to_invpc, label='$f^{isco}$')

plt.plot(f_gw/ms.hz_to_invpc, t_of_f(f_gw), label='$t(f)$')
plt.plot(f_gw/ms.hz_to_invpc, (t[-1] - 5. * (8*np.pi*f_gw)**(-8./3.) * sp.m_chirp**(-5./3.)), label='$t(f)^{analytic}$')

Phit = np.cumsum([quad(lambda t: omega_gw(t), t[i-1], t[i], limit=500, epsrel=1e-13, epsabs=1e-13)[0] if not i == 0 else 0. for i in range(len(t)) ])
Phi = Phit - Phit[-1]
Phi0 = - 2.*(8.*np.pi*sp.m_chirp*f_gw)**(-5./3.) + 2.*(8.*np.pi*sp.m_chirp*f_isco)**(-5./3.)

plt.plot(f_gw/ms.hz_to_invpc, Phi, label=r'$\Phi^{code}$')
plt.plot(f_gw/ms.hz_to_invpc, Phi0, label=r'$\Phi^{analytic}$')
plt.plot(f_gw/ms.hz_to_invpc, Phi - Phi0, label=r'$\Delta\Phi$')

tpt = 2.*np.pi*f_gw * (t - t_c)
tpt0 = -5./4. * (8.*np.pi*sp.m_chirp*f_gw)**(-5./3.)

plt.plot(f_gw/ms.hz_to_invpc, tpt, label=r'$2\pi ft^{code}$')
plt.plot(f_gw/ms.hz_to_invpc, tpt0, label=r'$2\pi ft^{analytic}$')
plt.plot(f_gw/ms.hz_to_invpc, tpt - tpt0, label=r'$\Delta2\pi ft$')
#plt.plot(f_gw/ms.hz_to_invpc, 2.*np.pi*f_gw*np.abs(t_c-t_c0), label=r'$\omega_{gw}\Delta t_c$')

PhiTild =  tpt - Phi
PhiTild0 = tpt0 - Phi0

plt.plot(f_gw/ms.hz_to_invpc, PhiTild, label=r'$\tilde{\Phi}_0$')
plt.plot(f_gw/ms.hz_to_invpc, PhiTild0, label=r'$\tilde{\Phi}^{analytic}_0$')
plt.plot(f_gw/ms.hz_to_invpc, PhiTild - PhiTild0, label=r'$\Delta\tilde{\Phi}$')

plt.xscale('log'); plt.xlabel('f / Hz')
plt.yscale('symlog')
plt.legend(); plt.grid()


plt.figure()

plt.axvline(t_c0/ms.year_to_pc, label='$t_c^{analytic}$')

Ra = (256./5. * sp.m_red * sp.m_tot**2 * (t_c - t))**(1./4.)
plt.plot(t/ms.year_to_pc, R, label='$R^{code}$')
plt.plot(t/ms.year_to_pc, Ra, label='$R^{analytic}$')
plt.plot(t/ms.year_to_pc, np.abs(Ra - R), label='$\Delta R$')

Phi = Phit
Phi0 = -2.* (1./5.*(t_c - t)/ sp.m_chirp)**(5./8.)
plt.plot(t/ms.year_to_pc, Phi, label='$\Phi^{code}$')
plt.plot(t/ms.year_to_pc, Phi0  - Phi0[0] + Phi[0], label='$\Phi^{analytic}$')
plt.plot(t/ms.year_to_pc, np.abs(Phi0 - Phi0[0] - Phi + Phi[0]), label='$\Delta\Phi$')

f_gw0 = 1./8./np.pi * 5**(3./8.) * sp.m_chirp**(-5./8.) * (t_c-t)**(-3./8.) / (1.+sp.z)
plt.plot(t/ms.year_to_pc, omega_s/np.pi, label='$f_{gw}$')
plt.plot(t/ms.year_to_pc, f_gw0, label='$f_{gw}^{analytic}$')
plt.plot(t/ms.year_to_pc, np.abs(omega_s/np.pi - f_gw0), label='$\Delta f_{gw}$' )


plt.xlabel('t / year'); #plt.xscale('log')
plt.yscale('log')
plt.legend(); plt.grid()


plt.figure()
f_gw0 = omega_s/np.pi
f_gw, h, Psi, __, PsiTild, __ = waveform.h_plus(t, omega_s, R, sp, dbg=True)

Psi0 = 2.*np.pi*f_gw0 * (t_c0 + sp.D) - np.pi/4. + 3./4. * (8.*np.pi*sp.m_chirp*f_gw0)**(-5./3.)
plt.plot(f_gw/ms.hz_to_invpc, Psi, label=r'$\Psi^{code}$')
plt.plot(f_gw0/(1.+sp.z)/ms.hz_to_invpc, Psi0, label=r'$\Psi^{analytic}$')

h0 = 1./sp.D * sp.m_chirp_redshifted**(5./6.)*(f_gw0/(1.+sp.z))**(-7./6.)
plt.plot(f_gw/ms.hz_to_invpc, h, label=r'$h^{code}$')
plt.plot(f_gw0/(1.+sp.z)/ms.hz_to_invpc, h0, label=r'$h^{analytic}$')

plt.plot(f_gw/ms.hz_to_invpc, PhiTild, label=r'$\tilde{\Phi}_0$')
plt.plot(f_gw0/(1.+sp.z)/ms.hz_to_invpc, PhiTild0, label=r'$\tilde{\Phi}^{analytic}_0$')

plt.xlabel('f / Hz')
plt.xscale('log')
plt.yscale('log')
plt.legend(); plt.grid()


plt.show()

