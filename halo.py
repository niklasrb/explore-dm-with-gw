import numpy as np
import cosmo
from scipy.interpolate import interp1d
from scipy.integrate import quad, simps
from scipy.misc import derivative
#import matplotlib.pyplot as plt
import collections


class DMHalo:

    def __init__(self, rho_0):
        self.r_min = 0.
        self.rho_0 = rho_0

    def density(self, r):
        return np.where(r > self.r_min, self.rho_0, 0.)

    def mass(self, r):
        integrand = lambda r: r**2 *self.density(r)
        if not isinstance(r, (collections.Sequence, np.ndarray)):
            return 4.*np.pi*quad(integrand, 0., r)[0]
        m_0 = quad(integrand, 0., r[0])[0]
        subIntervals = [quad(integrand, r[i-1], r[i])[0] if i > 0 else m_0 for i in range(len(r))]
        return 4.*np.pi*(np.cumsum(subIntervals))


class NFW(DMHalo):

    def __init__(self, rho_s, r_s):
        DMHalo.__init__(self, rho_s)
        self.r_s = r_s

    def density(self, r):
        return np.where(r > self.r_min, self.rho_0 / (r/self.r_s) / (1. + r/self.r_s)**2, 0.)

    def mass(self, r):
        return np.where(r > self.r_min, 4*np.pi*self.rho_0 * self.r_s**3 *(np.log((self.r_s+r)/(self.r_s+self.r_min)) + self.r_s/(self.r_s + r) - self.r_s/(self.r_s + self.r_min)), 0. )

    def FromHaloMass(M_vir, z_f):
        A_200 = 5.71; B_200 = -0.084; C_200 = -0.47; M_piv = 1./0.7 *1e14* 4.8e-14 # solar mass to pc
        c_200 = A_200 * (M_vir/M_piv)**B_200 * (1. + z_f)**C_200
        rho_crit_0 = 3*cosmo.hubble_const**2 / 8./np.pi
        Omega_m = cosmo.Omega_m(z_f)
        rho_m = cosmo.Omega_0_m*rho_crit_0 * (1.+z_f)**3
        Delta_vir = 18.*np.pi**2 *(1. + 0.4093 * (1/Omega_m - 1.)**0.9052)
        r_vir = (3.*M_vir / (4.*np.pi * Delta_vir * rho_m))**(1./3.)
        r_s = r_vir/c_200
        f = np.log(1+c_200) - c_200/(1.+c_200)
        rho_s = 1/3./f * Delta_vir * rho_m * c_200**3
        #print("Halo parameters: ","r_s=", r_s,"rho_s ", rho_s * 1.414e-9) # 1/pc^2 to g/cm^3
        return NFW(rho_s, r_s)

class SpikedNFW(NFW):

    def __init__(self, rho_s, r_s, r_spike, alpha):
        NFW.__init__(self, rho_s, r_s)
        self.alpha= alpha
        self.r_spike = r_spike
        self.rho_spike = rho_s * r_s/r_spike / (1+r_spike/r_s)**2

    def density(self, r):
        return np.where(r < self.r_spike, \
                        np.where(r > self.r_min, self.rho_spike * (self.r_spike/r)**self.alpha, 0.), \
                        super().density(r))

    def mass(self, r):
    
        return np.where(r < self.r_spike, \
                        np.where(r > self.r_min, 4*np.pi*self.rho_spike*self.r_spike**self.alpha * (r**(3.-self.alpha) - self.r_min**(3.-self.alpha)) / (3.-self.alpha), 0.), \
                        4*np.pi*self.rho_spike*self.r_spike**self.alpha * (self.r_spike**(3.-self.alpha)-self.r_min**(3.-self.alpha)) / (3.-self.alpha) + \
                            4*np.pi*self.rho_0 * self.r_s**3 *(np.log((self.r_s+r)/(self.r_s+self.r_spike)) + self.r_s/(self.r_s + r) - self.r_s/(self.r_s + self.r_spike)) )

    def FromNFW(nfw, M_bh, alpha):
        r = np.geomspace(1e-3*nfw.r_s, 1e3*nfw.r_s)
        M_to_r = interp1d(nfw.mass(r), r, kind='cubic', bounds_error=True)
        r_h = M_to_r(2.* M_bh)
        r_spike = 0.2*r_h
        return SpikedNFW(nfw.rho_0, nfw.r_s, r_spike, alpha)


class Hernquist(DMHalo):

    def __init__(self, rho_0, r_s):
        DMHalo.__init__(self, rho_0)
        self.r_s = r_s

    def density(self, r):
        return np.where(r > self.r_min, self.rho_0 / (r/self.r_s) / (1. + r/self.r_s)**3, 0.)

    def mass(self, r):
        return np.where(r > self.r_min, 4*np.pi*self.rho_0 * self.r_s**3 *( (2.*self.r_min/self.r_s + 1.)/2./(1.+self.r_min/self.r_s)**2  - (2.*r/self.r_s + 1.)/2./(1.+r/self.r_s)**2  ), 0. )

class Spike(DMHalo):

    def __init__(self, rho_s, r_spike, alpha):
        DMHalo.__init__(self, rho_s)
        self.alpha= alpha
        self.r_spike = r_spike

    def density(self, r):
        return np.where(r > self.r_min, self.rho_0 * (self.r_spike/r)**self.alpha, 0.)

    def mass(self, r):
        return np.where(r > self.r_min, 4*np.pi*self.rho_0*self.r_spike**self.alpha * (r**(3.-self.alpha) - self.r_min**(3.-self.alpha)) / (3.-self.alpha), 0.)


class DynamicSS(DMHalo):

    def __init__(self, Eps_grid, f_grid, potential):
        DMHalo.__init__(self, 1.)
        self.Eps_grid = Eps_grid
        self.f_grid = f_grid
        self.potential = potential

    def f(self, Eps):
        return np.interp(Eps, self.Eps_grid, self.f_grid)

    def density(self, r):
        v_max = np.sqrt(2*self.potential(r))
        if not isinstance(r, (collections.Sequence, np.ndarray)):
            return 4.*np.pi*quad(lambda v: v**2 * self.f(self.potential(r) - v**2 /2.), 0., v_max, limit=200)[0]

        return 4.*np.pi*np.array([quad(lambda v: v**2 * self.f(self.potential(r[i]) - v**2 /2.), 0., v_max[i], limit=200)[0] for i in range(len(r))])


    # TODO: Add the option to calculate it without external potential
    def FromStatic(Eps_grid, halo, extPotential):
        # find the right r grid for calculations, such that the potential encompasses the Eps_grid
        left = -5.; right=5.
        while extPotential(10**left) < Eps_grid[-1] and left > -20:
            left -= 1
        while extPotential(10.**right) > Eps_grid[0] and right < 30:
            right += 1
        r = np.geomspace(10.**left, 10.**right, 5000)
        # calculate f according to Eddington inversion
        Phi = extPotential(r)

        drho_dphi = interp1d(Phi, [derivative(halo.density, r, r/1e4)/derivative(extPotential, r, r/1e4) for r in r], kind='cubic', fill_value=(0.,0.), bounds_error=False)
        integrals = np.array([quad(lambda p: drho_dphi(p)/np.sqrt(Eps-p), 0., Eps, limit=100)[0] for Eps in Eps_grid])
        integrals_of_Eps = interp1d(Eps_grid, integrals, kind='cubic', bounds_error=False, fill_value='extrapolate')
        f_grid = 1./np.sqrt(8.)/np.pi**2 * np.array([derivative(integrals_of_Eps, Eps, Eps/1e4) for Eps in Eps_grid])
        f_grid = np.where(f_grid < 0., 0., f_grid)
        #print("f=", f_grid)

        '''  # Second possible method (seemed worse numerically)
        d2rho_dphi2 = interp1d(Phi, np.array([ derivative(halo.density, r, r/1e4, n=2) /derivative(extPotential, r, r/1e4)**2 + derivative(halo.density, r, r/1e4)/derivative(extPotential, r, r/1e4, n=2)  for r in r]) , \
                            kind='cubic', fill_value=(0.,0.), bounds_error=False)
        f_grid = 1./np.sqrt(8.)/np.pi**2 * np.array([ quad(lambda p: d2rho_dphi2(p) / np.sqrt(E - p), 0., E, limit=100)[0] for E in Eps_grid])
        print("f=", f_grid)
        '''
        return DynamicSS(Eps_grid, f_grid, extPotential)

