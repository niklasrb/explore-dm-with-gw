import numpy as np
from scipy.integrate import quad

hubble_const = 2.3e-10   # in 1/pc
Omega_0_m = 0.3111
Omega_0_L = 0.6889

def HubbleLaw(d_lum):
    return hubble_const * d_lum

def HubbleParameter(z):
    return hubble_const * np.sqrt(Omega_0_L)/ np.tanh(np.arcsinh(np.sqrt(Omega_0_L/Omega_0_m/(1.+z)**3) ))

def CriticalDensity(z):
    H = HubbleParameter(z)
    return  3.*H**2 / 8. / np.pi

def Omega_m(z):
    rho_crit = CriticalDensity(z)
    return 1. - 3*hubble_const**2 * Omega_0_L / (8. * np.pi * rho_crit)

