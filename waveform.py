import merger_system as ms
import numpy as np
from scipy.interpolate import UnivariateSpline, interp1d
from scipy.integrate import quad


def h_plus(t, omega_s, R, sp, dbg=False, acc=1e-13):

    # First, obtain mapping of gw frequency and time
    f_gw = omega_s / np.pi
    t_of_f = interp1d(f_gw, t, kind='cubic', bounds_error=False, fill_value='extrapolate')

    # Next, get the accumulated phase Phi
    omega_gw= UnivariateSpline(t, 2*omega_s, ext=1, k=5 )
    Phi = np.cumsum([quad(lambda t: omega_gw(t), t[i-1], t[i], limit=500, epsrel=acc, epsabs=acc)[0] if not i == 0 else 0. for i in range(len(t)) ])

    # and the derivative of omega_gw
    domega_gw= omega_gw.derivative()

    # Calculate PhiTilde
    Phi = Phi - Phi[-1]
    t_c= (t[-1] + 5./256. * R[-1]**4/sp.m_tot**2 / sp.m_red)
    tpt = 2.*np.pi*f_gw* (t- t_c)
    PhiTild = tpt - Phi

    # Now compute the time-dependant amplitude A
    A = 1./sp.D * 4. *sp.m_red_redshifted * omega_s**2 * R**2

    # The phase of the GW signal is given by the steady state aproximation
    Psi = 2.*np.pi*f_gw*sp.D + PhiTild - np.pi/4.

    # This gives us h on the f grid (accounting for redshift)
    h =  1./2. *  A * np.sqrt(2*np.pi * (1+sp.z)**2 / domega_gw(t_of_f(f_gw)))
            #* np.cos( Psi(t_of_f(f))) )

    if dbg:
        return f_gw/(1.+sp.z), h, Psi, t_of_f, PhiTild, A

    return f_gw/(1.+sp.z), h, Psi
